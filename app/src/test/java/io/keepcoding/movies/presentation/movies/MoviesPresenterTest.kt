package io.keepcoding.movies.presentation.movies

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.data.repository.MovieRepositoryImpl
import io.keepcoding.movies.domain.Movie
import io.keepcoding.movies.presentation.features.movies.MoviesFragment
import io.keepcoding.movies.presentation.features.movies.MoviesPresenter
import org.junit.Before
import org.junit.Test

/**
 * Created by costular on 11/02/2018.
 */
class MoviesPresenterTest {

    val view: MoviesFragment = mock()
    val repository: MovieRepositoryImpl = mock()
    lateinit var moviesPresenter: MoviesPresenter

    @Before
    fun setUp() {
        moviesPresenter = MoviesPresenter(view, repository)
    }

    @Test
    fun `load movies should call view loading`() {
        moviesPresenter.loadMovies()
        verify(view).showLoading(true)
        verify(view).showLoading(false)
    }

    @Test
    fun `load movies should call repository`() {
        moviesPresenter.loadMovies()
        verify(repository).getPopularMovies()
    }

    @Test
    fun `open movie should pass movie to view`() {
        moviesPresenter.openMovie(MovieEntity())
        verify(view).openMovieDetail(any())
    }

}