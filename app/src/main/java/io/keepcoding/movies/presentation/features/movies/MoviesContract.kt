package io.keepcoding.movies.presentation.features.movies

import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.domain.Movie

/**
 * Created by costular on 13/01/2018.
 */
interface MoviesContract {

    interface View {

        fun showLoading(isLoading: Boolean)

        fun showMovies(movies: List<MovieEntity>)

        fun openMovieDetail(movie: MovieEntity)

        fun showError(messageRes: Int)

        fun showError(message: String)

    }

    interface Presenter {

        fun loadMovies()

        fun openMovie(movie: MovieEntity)

    }

}