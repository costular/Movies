package io.keepcoding.movies.presentation.di.components

import android.content.Context
import dagger.Component
import io.keepcoding.movies.data.net.api.MovieService
import io.keepcoding.movies.data.repository.MovieRepositoryImpl
import io.keepcoding.movies.data.repository.datasource.MovieApiDataSource
import io.keepcoding.movies.data.repository.datasource.MovieLocalDataSource
import io.keepcoding.movies.presentation.MovieApp
import io.keepcoding.movies.presentation.di.modules.ApplicationModule
import io.keepcoding.movies.presentation.di.modules.NetworkModule
import io.keepcoding.movies.presentation.di.scopes.PerApplication

/**
 * Created by costular on 12/01/2018.
 */
@PerApplication
@Component(modules = [(ApplicationModule::class), (NetworkModule::class)])
interface ApplicationComponent {

    fun getContext(): Context
    fun getApplication(): MovieApp
    fun getMovieService(): MovieService
    fun getLocalDataSource(): MovieLocalDataSource
    fun getApiDataSource(): MovieApiDataSource
    fun getMovieRepository(): MovieRepositoryImpl

}