package io.keepcoding.movies.presentation.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import io.keepcoding.movies.data.db.MovieDatabase
import io.keepcoding.movies.data.db.MovieEntityMapper
import io.keepcoding.movies.data.db.MovieMapper
import io.keepcoding.movies.data.net.api.MovieService
import io.keepcoding.movies.data.repository.MovieRepositoryImpl
import io.keepcoding.movies.data.repository.datasource.MovieApiDataSource
import io.keepcoding.movies.data.repository.datasource.MovieLocalDataSource
import io.keepcoding.movies.presentation.MovieApp
import io.keepcoding.movies.presentation.di.scopes.PerApplication

/**
 * Created by costular on 12/01/2018.
 */
@Module
class ApplicationModule(private val application: MovieApp) {

    @PerApplication
    @Provides
    fun getApplication(): MovieApp = application

    @PerApplication
    @Provides
    fun getContext(): Context = application.applicationContext

    @PerApplication
    @Provides
    fun getMovieMapper(): MovieMapper = MovieMapper()

    @PerApplication
    @Provides
    fun getMovieEntityMapper(): MovieEntityMapper = MovieEntityMapper()

    @PerApplication
    @Provides
    fun getApp(context: Context): MovieDatabase = MovieDatabase.getInstance(context)

    @PerApplication
    @Provides
    fun getMovieApiDataSource(movieService: MovieService): MovieApiDataSource =
            MovieApiDataSource(movieService)

    @PerApplication
    @Provides
    fun getMovieLocalDataSource(movieDatabase: MovieDatabase) =
            MovieLocalDataSource(movieDatabase)

    @PerApplication
    @Provides
    fun getRepository(movieApiDataSource: MovieApiDataSource,
                      movieLocalDataSource: MovieLocalDataSource,
                      movieMapper: MovieMapper,
                      movieEntityMapper: MovieEntityMapper): MovieRepositoryImpl =
            MovieRepositoryImpl(movieApiDataSource, movieLocalDataSource, movieMapper, movieEntityMapper)

}