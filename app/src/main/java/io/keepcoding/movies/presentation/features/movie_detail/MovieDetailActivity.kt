package io.keepcoding.movies.presentation.features.movie_detail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.request.RequestOptions
import io.keepcoding.movies.R
import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.data.net.api.ImageManager
import io.keepcoding.movies.domain.Movie
import io.keepcoding.movies.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_movie_detail.*

/**
 * Created by costular on 16/01/2018.
 */
class MovieDetailActivity : BaseActivity() {

    companion object {
        const val PARAM_MOVIE = "movie"
    }

    lateinit var movie: MovieEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        setUpToolbar(true)
        setTitle("")

        movie = intent.getParcelableExtra(PARAM_MOVIE)

        if (movie == null) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
            finish()
        }

        val fragment = MovieDetailFragment()
        fragment.arguments = Bundle().apply { putParcelable(PARAM_MOVIE, movie) }

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit()

        setupViews()
    }

    private fun setupViews() {
        Glide.with(this)
                .load(ImageManager.getBackdropImage(movie.backdropPath))
                .apply(RequestOptions().apply {
                    centerCrop()
                    priority(Priority.HIGH)
                })
                .into(moviePoster)
    }

}