package io.keepcoding.movies.presentation.di.components

import dagger.Component
import io.keepcoding.movies.presentation.di.modules.MovieModule
import io.keepcoding.movies.presentation.di.scopes.PerActivity
import io.keepcoding.movies.presentation.features.movies.MoviesFragment

/**
 * Created by costular on 13/01/2018.
 */
@PerActivity
@Component(modules = [(MovieModule::class)], dependencies = [(ApplicationComponent::class)])
interface MovieComponent {

    fun inject(moviesFragment: MoviesFragment)

}