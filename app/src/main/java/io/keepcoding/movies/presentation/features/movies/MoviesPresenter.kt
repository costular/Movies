package io.keepcoding.movies.presentation.features.movies

import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.data.repository.MovieRepositoryImpl
import io.keepcoding.movies.domain.Movie
import io.keepcoding.movies.presentation.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by costular on 13/01/2018.
 */
class MoviesPresenter @Inject constructor(private val view: MoviesContract.View,
                                          private val movieRepositoryImpl: MovieRepositoryImpl) : BasePresenter(), MoviesContract.Presenter {

    override fun loadMovies() {
        addToDisposable(
                movieRepositoryImpl.getPopularMovies()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe { view.showLoading(true) }
                        .doOnNext { view.showLoading(false) }
                        .subscribeBy(
                                onNext = {
                                    view.showMovies(it)
                                },
                                onError = {
                                    view.showError(it.message!!)
                                }
                        )
        )
    }

    override fun openMovie(movie: MovieEntity) {
        view.openMovieDetail(movie)
    }

}