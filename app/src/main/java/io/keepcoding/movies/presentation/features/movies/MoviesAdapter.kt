package io.keepcoding.movies.presentation.features.movies

import android.content.Context
import android.graphics.Bitmap
import android.support.v7.graphics.Palette
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import io.keepcoding.movies.R
import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.data.net.api.ImageManager
import io.keepcoding.movies.domain.Movie
import kotlinx.android.synthetic.main.item_movie.view.*

/**
 * Created by costular on 12/01/2018.
 */
class MoviesAdapter(val body: (movie: MovieEntity) -> Unit) : RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    private val mutableMovies = mutableListOf<MovieEntity>()
    private lateinit var context: Context

    override fun onBindViewHolder(holder: MoviesViewHolder?, position: Int) {
        val movie = mutableMovies[position]
        holder?.bind(movie)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MoviesViewHolder {
        context = parent!!.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_movie, parent, false)
        return MoviesViewHolder(view)
    }

    override fun getItemCount(): Int = mutableMovies.size

    fun updateMovies(movies: List<MovieEntity>) {
        mutableMovies.clear()
        mutableMovies.addAll(movies)
        notifyDataSetChanged()
    }

    inner class MoviesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(movie: MovieEntity) {
            with(itemView) {
                Glide.with(context)
                        .asBitmap()
                        .load(ImageManager.getPosterImage(movie.posterPath))
                        .apply(RequestOptions().apply {
                            centerCrop()
                            priority(Priority.HIGH)
                        })
                        .listener(object : RequestListener<Bitmap>{
                            override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                resource?.let {
                                    Palette.from(resource).generate({ palette ->
                                        val vibrant = palette.vibrantSwatch
                                        vibrant?.let {
                                            titleBackground.setBackgroundColor(vibrant.rgb)
                                            movieTitle.setTextColor(vibrant.bodyTextColor)
                                        }
                                    })
                                }
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                        })
                        .into(moviePoster)

                movieTitle.text = movie.title

                itemView.setOnClickListener { body(mutableMovies[adapterPosition]) }
            }
        }
    }
}