package io.keepcoding.movies.presentation.features.movie_detail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.keepcoding.movies.R
import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.domain.Movie
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import kotlinx.android.synthetic.main.item_movie.*

/**
 * Created by costular on 16/01/2018.
 */
class MovieDetailFragment : Fragment() {

    lateinit var movie: MovieEntity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        movie = arguments!!.getParcelable(MovieDetailActivity.PARAM_MOVIE)
        setupViews()
    }

    private fun setupViews() {
        movieDetailTitle.text = movie.title
        movieRelease.text = movie.releaseDate
        movieRating.text = movie.voteAverage.toString()
        movieOverview.text = movie.overview
    }

}