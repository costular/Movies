package io.keepcoding.movies.presentation.di.modules

import dagger.Module
import dagger.Provides
import io.keepcoding.movies.presentation.di.scopes.PerActivity
import io.keepcoding.movies.presentation.features.movies.MoviesContract
import io.keepcoding.movies.presentation.features.movies.MoviesFragment

/**
 * Created by costular on 13/01/2018.
 */
@Module
class MovieModule(private val moviesFragment: MoviesFragment) {

    @PerActivity
    @Provides
    fun provideMovieView(): MoviesContract.View = moviesFragment

}