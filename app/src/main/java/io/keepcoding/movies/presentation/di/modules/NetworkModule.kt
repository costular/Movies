package io.keepcoding.movies.presentation.di.modules

import dagger.Module
import dagger.Provides
import io.keepcoding.movies.BuildConfig
import io.keepcoding.movies.data.net.api.MovieService
import io.keepcoding.movies.presentation.di.scopes.PerApplication
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by costular on 12/01/2018.
 */
@Module
class NetworkModule(private val url: String) {

    @PerApplication
    @Provides
    fun getRetrofit(okHttpClient: OkHttpClient): Retrofit =
            Retrofit.Builder()
                    .baseUrl(url)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

    @PerApplication
    @Provides
    fun getHttpClient(): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                    .addInterceptor { chain ->
                        val request = chain.request()
                        val originalHttpURL = request.url()

                        val finalURL = originalHttpURL.newBuilder()
                                .addQueryParameter("api_key", BuildConfig.TMDB_API_KEY)
                                .addQueryParameter("language", "es-ES")
                                .build()

                        val finalRequest = request.newBuilder()
                                .url(finalURL)
                                .build()

                        chain.proceed(finalRequest)
                    }
                    .build()

    @PerApplication
    @Provides
    fun getMovieService(retrofit: Retrofit): MovieService =
            retrofit.create(MovieService::class.java)

}