package io.keepcoding.movies.presentation

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*

/**
 * Created by costular on 12/01/2018.
 */
abstract class BasePresenter {

    private val compositeDisposables = CompositeDisposable()

    fun addToDisposable(disposable: Disposable) {
        compositeDisposables.add(disposable)
    }

    fun destroy() {
        compositeDisposables.clear()
    }

}