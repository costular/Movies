package io.keepcoding.movies.presentation.features.movies

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import io.keepcoding.movies.R
import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.domain.Movie
import io.keepcoding.movies.presentation.MovieApp
import io.keepcoding.movies.presentation.di.components.DaggerMovieComponent
import io.keepcoding.movies.presentation.di.modules.MovieModule
import io.keepcoding.movies.presentation.features.movie_detail.MovieDetailActivity
import kotlinx.android.synthetic.main.fragment_movies.*
import javax.inject.Inject

/**
 * Created by costular on 13/01/2018.
 */
class MoviesFragment : Fragment(), MoviesContract.View {

    @Inject
    lateinit var presenter: MoviesPresenter

    private lateinit var adapter: MoviesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setup()
    }

    private fun setup() {
        inject()
        moviesRecycler.layoutManager = GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)
        moviesRecycler.itemAnimator = DefaultItemAnimator()
        adapter = MoviesAdapter {
            val intent = Intent(activity, MovieDetailActivity::class.java)
            intent.apply {
                putExtra(MovieDetailActivity.PARAM_MOVIE, it)
            }
            startActivity(intent)
        }
        moviesRecycler.adapter = adapter
    }

    private fun inject() {
        DaggerMovieComponent.builder()
                .applicationComponent((activity!!.application as MovieApp).component)
                .movieModule(MovieModule(this))
                .build()
                .inject(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.loadMovies()
    }

    override fun showLoading(isLoading: Boolean) {
        loadingMovies.visibility = if(isLoading) View.VISIBLE else View.GONE
    }

    override fun showMovies(movies: List<MovieEntity>) {
        adapter.updateMovies(movies)
    }

    override fun openMovieDetail(movie: MovieEntity) {
        presenter.openMovie(movie)
    }

    override fun showError(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    override fun showError(messageRes: Int) {
        Toast.makeText(activity, messageRes, Toast.LENGTH_LONG).show()
    }

    override fun onStop() {
        presenter.destroy()
        super.onStop()
    }
}