package io.keepcoding.movies.presentation.di.scopes

import javax.inject.Scope

/**
 * Created by costular on 12/01/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication