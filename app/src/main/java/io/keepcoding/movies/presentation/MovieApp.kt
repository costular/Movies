package io.keepcoding.movies.presentation

import android.app.Application
import io.keepcoding.movies.presentation.di.components.DaggerApplicationComponent
import io.keepcoding.movies.presentation.di.modules.ApplicationModule
import io.keepcoding.movies.presentation.di.modules.NetworkModule

/**
 * Created by costular on 12/01/2018.
 */
class MovieApp : Application() {

    val component by lazy {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .networkModule(NetworkModule("https://api.themoviedb.org/3/"))
                .build()
    }

}