package io.keepcoding.movies.presentation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import io.keepcoding.movies.R
import io.keepcoding.movies.presentation.features.movies.MoviesFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Load fragment into activity
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, MoviesFragment())
                .commit()
    }

}
