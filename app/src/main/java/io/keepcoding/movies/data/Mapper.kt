package io.keepcoding.movies.data

/**
 * Created by costular on 12/02/2018.
 */
interface Mapper<in R, out T> {

    fun transform(r: R): T
    fun transformList(r: List<R>): List<T>

}