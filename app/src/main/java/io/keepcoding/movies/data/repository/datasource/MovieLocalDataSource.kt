package io.keepcoding.movies.data.repository.datasource

import android.graphics.Movie
import io.keepcoding.movies.data.db.MovieDatabase
import io.keepcoding.movies.data.entity.MovieEntity
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by costular on 12/01/2018.
 */
class MovieLocalDataSource @Inject constructor(val database: MovieDatabase){

    fun getPopularMovies(): Flowable<List<MovieEntity>> =
            database.movieDao()
                    .getAllMovies()

    fun insertMovies(movies: List<MovieEntity>) {
        database.movieDao()
                .insertMovies(movies)
    }

}