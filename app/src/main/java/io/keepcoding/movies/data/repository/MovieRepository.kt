package io.keepcoding.movies.data.repository

import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.domain.Movie
import io.reactivex.Flowable

/**
 * Created by costular on 12/01/2018.
 */
interface MovieRepository {

    fun getPopularMovies(): Flowable<List<MovieEntity>>

    fun searchMovies(query: String): Flowable<List<Movie>>

    fun getMovieDetail(movieId: Long): Flowable<Movie>

}