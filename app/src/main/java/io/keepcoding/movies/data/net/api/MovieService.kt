package io.keepcoding.movies.data.net.api

import io.keepcoding.movies.domain.Movie
import io.keepcoding.movies.domain.PaginatedResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by costular on 12/01/2018.
 */
interface MovieService {

    @GET("discover/movie")
    fun popularMovies(@Query("sort_by") sortBy: String = "popularity.desc"): Flowable<PaginatedResponse<Movie>>

    @GET("discover/movie")
    fun highRatedMovies(@Query("sort_by") sortBy: String = "vote_average.desc"): Flowable<PaginatedResponse<Movie>>

    @GET("discover/movie")
    fun newestMovies(@Query("sort_by") sortBy: String = "release_date.desct"): Flowable<PaginatedResponse<Movie>>

    @GET("movie/{movie_id}")
    fun movieDetail(@Path("movie_id") movieId: Long): Flowable<Movie>

    @GET("search/movie")
    fun searchMovies(@Query("query") query: String): Flowable<PaginatedResponse<Movie>>

}