package io.keepcoding.movies.data.db

import io.keepcoding.movies.data.Mapper
import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.domain.Movie

/**
 * Created by costular on 12/02/2018.
 */
class MovieMapper : Mapper<MovieEntity, Movie> {

    override fun transform(r: MovieEntity): Movie =
            Movie(r.id, r.video, r.voteAverage, r.title, r.popularity, r.posterPath, r.releaseDate, r.backdropPath, r.runtime, r.overview)

    override fun transformList(r: List<MovieEntity>): List<Movie> =
            r.map { transform(it) }

}