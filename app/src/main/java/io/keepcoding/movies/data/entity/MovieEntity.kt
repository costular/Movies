package io.keepcoding.movies.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by costular on 21/01/2018.
 */
@Parcelize
@Entity(tableName = MovieEntity.TABLENAME)
data class MovieEntity(
        @PrimaryKey
        var id: String = "",
        var video: Boolean = false,
        @ColumnInfo(name = "vote_average")
        var voteAverage: Double = 0.0,
        var title: String = "",
        var popularity: Double = 0.0,
        @ColumnInfo(name = "poster_path")
        var posterPath: String = "",
        @ColumnInfo(name = "release_date")
        var releaseDate: String = "",
        @ColumnInfo(name = "backdrop_path")
        var backdropPath: String = "",
        var runtime: Int = 0,
        var overview: String = ""
): Parcelable {
        companion object {
            const val TABLENAME = "movies"
        }
}