package io.keepcoding.movies.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.keepcoding.movies.data.entity.MovieEntity
import io.reactivex.Flowable

/**
 * Created by costular on 21/01/2018.
 */
@Dao
abstract class MovieDao {

    @Query("SELECT * FROM ${MovieEntity.TABLENAME}")
    abstract fun getAllMovies(): Flowable<List<MovieEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertMovies(movies: List<MovieEntity>)

}