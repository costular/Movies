package io.keepcoding.movies.data.net.api

/**
 * Created by costular on 12/01/2018.
 */
object ImageManager {

    const val BASE_POSTER_PATH = "http://image.tmdb.org/t/p/w342"
    const val BASE_BACKDROP_PATH = "http://image.tmdb.org/t/p/w780"

    fun getPosterImage(imageId: String): String =
            BASE_POSTER_PATH + imageId

    fun getBackdropImage(imageId: String): String =
            BASE_BACKDROP_PATH + imageId

}