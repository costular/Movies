package io.keepcoding.movies.data.repository

import android.util.Log
import io.keepcoding.movies.data.db.MovieEntityMapper
import io.keepcoding.movies.data.db.MovieMapper
import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.data.repository.datasource.MovieApiDataSource
import io.keepcoding.movies.data.repository.datasource.MovieLocalDataSource
import io.keepcoding.movies.domain.Movie
import io.reactivex.Flowable

/**
 * Created by costular on 12/01/2018.
 */
class MovieRepositoryImpl(private val movieApiDataSource: MovieApiDataSource,
                          private val movieLocalDataSource: MovieLocalDataSource,
                          private val movieMapper: MovieMapper,
                          private val movieEntityMapper: MovieEntityMapper) : MovieRepository {

    override fun getPopularMovies(): Flowable<List<MovieEntity>> =
            Flowable.merge(
                    getLocalMovies(),
                    getApiMovies())
                    .filter { it.isNotEmpty() }

    override fun searchMovies(query: String): Flowable<List<Movie>> =
            movieApiDataSource.searchMovies(query)

    override fun getMovieDetail(movieId: Long): Flowable<Movie> =
            movieApiDataSource.getMovieDetail(movieId)

    private fun getLocalMovies(): Flowable<List<MovieEntity>> =
            movieLocalDataSource.getPopularMovies()
                    .onErrorResumeNext{_: Throwable -> Flowable.just(arrayListOf()) }

    private fun getApiMovies(): Flowable<List<MovieEntity>> =
            movieApiDataSource.getPopularMovies()
                    .map { movieEntityMapper.transformList(it) }
                    .doOnNext { insertMovies(it) }

    private fun insertMovies(movies: List<MovieEntity>) {
        movieLocalDataSource.insertMovies(movies)
    }
}