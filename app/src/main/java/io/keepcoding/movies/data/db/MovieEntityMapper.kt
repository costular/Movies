package io.keepcoding.movies.data.db

import io.keepcoding.movies.data.Mapper
import io.keepcoding.movies.data.entity.MovieEntity
import io.keepcoding.movies.domain.Movie

/**
 * Created by costular on 12/02/2018.
 */
class MovieEntityMapper : Mapper<Movie, MovieEntity> {

    override fun transform(r: Movie): MovieEntity =
            MovieEntity(r.id, r.video, r.voteAverage, r.title, r.popularity, r.posterPath, r.releaseDate, r.backdropPath, r.runtime, r.overview)

    override fun transformList(r: List<Movie>): List<MovieEntity> =
            r.map { transform(it) }

}