package io.keepcoding.movies.data.repository.datasource

import io.keepcoding.movies.data.net.api.MovieService
import io.keepcoding.movies.domain.Movie
import io.reactivex.Flowable
import javax.inject.Inject

/**
 * Created by costular on 12/01/2018.
 */
class MovieApiDataSource @Inject constructor(val movieService: MovieService) {

    fun getPopularMovies(): Flowable<List<Movie>> =
            movieService.popularMovies().map { it.results }

    fun searchMovies(query: String): Flowable<List<Movie>> =
            movieService.searchMovies(query).map { it.results }

    fun getMovieDetail(movieId: Long): Flowable<Movie> =
            movieService.movieDetail(movieId)

}