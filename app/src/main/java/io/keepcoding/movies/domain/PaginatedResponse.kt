package io.keepcoding.movies.domain

import com.google.gson.annotations.SerializedName

/**
 * Created by costular on 12/01/2018.
 */
data class PaginatedResponse<out T>(
        val page: Int,
        @SerializedName("total_results")
        val totalResults: Int,
        @SerializedName("total_pages")
        val totalPages: Int,
        val results: List<T>
)