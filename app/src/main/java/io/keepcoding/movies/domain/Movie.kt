package io.keepcoding.movies.domain

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by costular on 12/01/2018.
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class Movie(
        val id: String,
        val video: Boolean,
        @SerializedName("vote_average")
        val voteAverage: Double,
        val title: String,
        val popularity: Double,
        @SerializedName("poster_path")
        val posterPath: String,
        @SerializedName("release_date")
        val releaseDate: String,
        @SerializedName("backdrop_path")
        val backdropPath: String,
        val runtime: Int,
        val overview: String = ""
): Parcelable