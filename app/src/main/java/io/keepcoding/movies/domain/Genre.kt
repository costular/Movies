package io.keepcoding.movies.domain

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by costular on 12/01/2018.
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class Genre(
        val id: String,
        val name: String
): Parcelable